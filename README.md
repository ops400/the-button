# The Button
The Button é um jogo no qual você tem que apertar um botão o máximo que poder dentro de 30 segundos.<br>
Quando o tempo acabar sua pontuação sera guardada em um placar com 10 posições, então você pode competir com seus amigos para ver quem tem a maior pontuação! Pera você não tem amigos? Não tema! Jogue o máximo que poder para bater sua ultima pontuação.<br>
The Button é realmente um jogo para todas as ocasiões!
## Informações Técnicas
Esta é a versão 1.0.0 de The Button lançada em 27 de Julho de 2023.<br>
The Button foi escrito no Visual Studio Code, com sua logicá sendo escrita em JavaScript, sua formatação em HTML e a estilização em CSS.<br> 
## Créditos
- Programação por:
    - Guilherme Camara
    - João Guilherme
    - Henrique Freitas
- Estilização CSS
    - Lenny Cesar