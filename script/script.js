/*
  ╔══════════════════════════╗
  ║                          ║
  ║      Nosso Primeiro      ║
  ║          Jogo            ║
  ║                          ║
  ║         V 1.0.0          ║
  ║                          ║
  ║  Autores:                ║
  ║    - Guilherme Camara    ║
  ║    - João Guilherme      ║
  ║    - Henrique Freitas    ║
  ║    - Lenny Cesar         ║
  ║                          ║
  ║  Data de criação:        ║
  ║     27 de julho de 2023  ║
  ║                          ║
  ╚══════════════════════════╝
*/

var placar = new Array()
var n = 1;
var pontos = 0;
var intervalo;
var mil;
var seg;
var debug;
var segundos = 0;
var milesimos = 0;
var pts = window.document.getElementById("pontos");
var botao = window.document.getElementById("botao");
var btnEvent = botao.addEventListener("mousedown", repeticao);
var time = window.document.getElementById("tempo");
var btnReset = window.document.getElementById("btnResetar");
//coloquei window. para ir com as normas estabelecidas pelo Gui Camara, mas eu acho q nn precisa
var altura = 0.0;//declarei ambos com 0.0 pq Math.random() gera numeros quebrados ent precisa inicar a var com float
var largura = 0.0;//bom em python é assim então aq deve ser tbm
//declarei altura e largura fora de repeticao pq nn gosto de var dentro de function
btnReset.setAttribute('disabled', 'disabled');
var lugar = [];
lugar[0] = window.document.getElementById("l1");
lugar[1] = window.document.getElementById("l2");
lugar[2] = window.document.getElementById("l3");
lugar[3] = window.document.getElementById("l4");
lugar[4] = window.document.getElementById("l5");
lugar[5] = window.document.getElementById("l6");
lugar[6] = window.document.getElementById("l7");
lugar[7] = window.document.getElementById("l8");
lugar[8] = window.document.getElementById("l9");
lugar[9] = window.document.getElementById("l10");
//eu sei q deve ter uma solução melhor mais oq deu na telha agr
var divNome = window.document.getElementById("inserirNome");
divNome.style.visibility='hidden';
function repeticao() {
    altura = Math.floor(Math.random() * 720); // vai se fuder jão guilherme você se acha por fazer isso, mas eu faço isso em três minutos em C
    largura = Math.floor(Math.random() * 1200);
    botao.style.marginTop = `${altura}px`;
    botao.style.marginLeft = `${largura}px`;
    pontos++;
    pts.innerHTML = pontos;
    if (n == 1) {
        aux();
        n++;
    }
}

function stop() {
    guardarInfo();
    btnReset.removeAttribute('disabled');
    botao.setAttribute('disabled', 'disabled');
}

function aux() {
    intervalo = setTimeout(stop, 30000);
    seg = setInterval(segundo, 1000);
    mil = setInterval(milesimo, 10);
    debug = setInterval(showInfo, 10000)
}

function segundo() {
    if (segundos != 30) {
        segundos++;
        time.innerHTML = segundos;
    }
}

function milesimo() {
    if (segundos != 30) {
        if (milesimos != 99) {
            milesimos++;
            time.innerHTML = segundos + ":" + milesimos;
        } else {
            milesimos = 0;
        }
    }
}

function reset() {
    clearInterval(seg);
    clearInterval(mil);
    clearTimeout(intervalo);
    milesimos = 0;
    segundos = 0;
    pontos = 0;
    pts.innerHTML = pontos;
    btnReset.setAttribute('disabled', 'disabled');
    botao.removeAttribute('disabled');
    aux();
    //eu chamei a função auxiliar novamente pq nas linhas 64, 65 e 66 nós só limpamos o setInterval e o setTimeou e não os reicializamos eles
    //dai precisa limpar eles e reinicializar eles novamente
}

function guardarInfo(){
    placar.push(pontos);
    placar.sort(function(a,b){return b - a});
    //esta porra funciona porque o js toma o index da array como argumento na função
}

function showInfo(){
    for(var i=0;i < 10; i++){
        if(typeof placar[i] == 'undefined'){ 
            console.log("aqui");
            lugar[i].innerHTML = "";
        }
        else lugar[i].innerHTML = placar[i];   
    }
    console.log(placar);
}